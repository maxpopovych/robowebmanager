1. Install docker https://docs.docker.com/install/linux/docker-ce/ubuntu/
2. Add to hosts:

```$xslt
## Roboto Transfer

127.0.0.1	robotomanager.local
127.0.0.1 	www.robotomanager.local
```

## How to run project

Run websocket:

```$xslt
laravel-echo-server start
```