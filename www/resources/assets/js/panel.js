import Vue from 'vue';
import injector from 'vue-inject';
import axios from 'axios';
import Swal from 'sweetalert2';

import listTable from './components/listTable.vue';
//import test from './components/test.vue';



Vue.use(injector);


injector.constant('swal', Swal);
injector.constant('axios', axios);
// injector.service('lang', {
//     test: 'testss'
// });
Vue.component('list-table', listTable);

// Vue.component('vuetable', Vuetable);
// Vue.component('vuetable-pagination', VuetablePagination)
// Vue.component('vuetable-pagination-dropdown', VuetablePaginationDropdown)
// Vue.component('vuetable-pagination-info', VuetablePaginationInfo)
// Vue.component('vuetable-pagination-info-mixin',VuetablePaginationInfoMixin)
// Vue.use(Vuetable);
//

// window.Swal = Swal;
window.Vue = Vue;
window.onload = function () {

}