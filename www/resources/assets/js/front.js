
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

var Vue = require('vue');

const singIn = new Vue({
    el: '#page-login',
    data: {
        api: null,
        error: null,
        login: null,
        password: null,
        rememberMe: false
    },
    mounted: function() {
        this.api = this.$el.querySelector('form').getAttribute('data-url');
    },
    methods: {
        signIn: function(event) {

            event.preventDefault();
            let buttonTarget = event.currentTarget;
            let panelLogin = this.$el;
            let app = this;

            buttonTarget.disabled = true;
            panelLogin.classList.add('loader');

            axios({
                method: 'post',
                url: app.api,
                data: {
                    login: app.login,
                    password: app.password,
                    rememberMe: app.rememberMe
                }
            })
            .then(function (response) {
                let data = response.data;
                if (data.success == true && data.redirect) {
                    window.location.replace(data.redirect);
                } else {
                    if (data.error) {
                        app.error = data.error;
                    }
                    panelLogin.classList.remove('loader');
                    buttonTarget.disabled = false;
                }
            })
            .catch(function (error) {
                app.error = 'Undefined error';
            });

        }
    }
});