<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    {!! Application::head()->generate(); !!}
    <style type="text/css">
        [v-cloak] {
            display: none;
        }
    </style>
</head>
<body class="layout-full">
    @yield('body')
    {!! Application::footer()->generate(); !!}
</body>
</html>