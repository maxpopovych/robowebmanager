@extends('body')

@section('body')

    <main id="page-login" class="page-login" v-cloak>
        <div class="page vertical-align text-center" data-animsition-in="fade-in" data-animsition-out="fade-out">
            <div class="page-content vertical-align-middle">
                <div class="panel">
                    <div class="panel-body">
                        <div class="brand">
                            <h2 class="brand-text font-size-18">RobotoManager</h2>
                        </div>
                        <div class="errors" v-if="error">
                            @{{ error }}
                        </div>
                        <form method="post" action="#" autocomplete="off" data-url="{{ url('/authenticate') }}">
                            <div class="form-group form-material floating" data-plugin="formMaterial">
                                <input type="email" class="form-control" name="email" v-model="login" />
                                <label class="floating-label">{{ __('Email') }}</label>
                            </div>
                            <div class="form-group form-material floating" data-plugin="formMaterial">
                                <input type="password" class="form-control" name="password" v-model="password" />
                                <label class="floating-label">{{ __('Password') }}</label>
                            </div>
                            <div class="form-group clearfix">
                                <div class="checkbox-custom checkbox-inline checkbox-primary checkbox-lg float-left">
                                    <input type="checkbox" id="inputCheckbox" name="remember" v-model="rememberMe">
                                    <label for="inputCheckbox">{{ __('Remember me') }}</label>
                                </div>
                            </div>
                            <button type="button" class="btn btn-primary btn-block btn-lg mt-40" v-on:click="signIn">{{ _i('Sign in') }}</button>
                        </form>
                    </div>
                </div>
                <footer class="page-copyright page-copyright-inverse">
                    <p>{{ __('© 2018. All right reserved.') }}</p>
                </footer>
            </div>
        </div>
    </main>

@stop