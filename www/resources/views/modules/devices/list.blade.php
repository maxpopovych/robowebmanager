@extends('panel')

@section('content')

    <script>

        window.lang = {
            pagination_info: 'Displaying {from} to {to} of {total} items',
            remove_selected: 'Remove'
        };

    </script>

    <h2>Devices</h2>

    <div id="table-list">
        <list-table :list-data="listTableArgs"></list-table>
        <div class="actions text-right">
            <a href="{{action('DeviceController@create')}}" class="btn btn-primary">Add new device</a>
        </div>
    </div>

    <script>

        new Vue({
            el: '#table-list',
            dependencies: [ 'swal', 'axios' ],
            data: {
                listTableArgs: {
                    url: "{{ $listTable['url'] }}",
                    perPage: "{{ $listTable['per_page'] }}",
                    searchForm: true,
                    fields: [
                        {
                            title: 'Id',
                            name: 'id'
                        },
                        {
                            title: 'Name',
                            name: 'name'
                        },
                        {
                            title: 'Active Task',
                            name: 'active_task_name'
                        }
                    ],
                    actions: {
                        edit: {
                            show: true,
                            url: "devices/%{id}/edit"
                        },
                        delete: {
                            show: true,
                            url: "devices/%{id}"
                        },
                        additional: {
                            set_task: {
                                title: 'Set Active task',
                                method: function (data, index, event) {

                                    event.swal({
                                        title: 'Select task to run',
                                        input: 'select',
                                        inputOptions: {
                                            '': 'Reset',
                                            @foreach($tasks as $task)
                                            '{{$task['id']}}': '{{$task['name']}}',
                                            @endforeach
                                        },
                                    }).then(function(selected){
                                        event.axios.post('{{action('DeviceController@setTask')}}', {
                                            id_device: data.id,
                                            id_task: parseInt(selected.value)
                                        }).then(function(response){
                                            var success = response.data.success;
                                            if ( success === true ) {
                                                event.swal( 'Task changed successfull!', 'You changed it!', 'success' );
                                                event.$refs.vuetable.refresh();
                                            }
                                        });
                                    });


                                }
                            }
                        }
                    },
                }
            }
        });
    </script>

@stop