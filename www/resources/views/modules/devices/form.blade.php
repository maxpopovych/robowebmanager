@extends('panel')

@section('content')

    @if(isset($message))
        <div class="alert alert-info" role="alert">
            {{$message}}
        </div>
    @endif

    @if(isset($device))
        <h2>Edit Device</h2>
        {!! Form::horizontal($device)->action('DeviceController@update', ['id' => $device->id])->method('PUT') !!}
    @else
        <h2>Add new device</h2>
        {!! Form::horizontal(null)->action('DeviceController@store')->method('POST') !!}
    @endif

    <div class="row">
        <div class="col-md-12">
            {!! Form::group()->fieldSize(8)->text('name')->label(__('Name'))->placeholder(__('Name'))->required(true) !!}
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 text-right">
            @if(isset($device))
                {!! Form::submit('submit', __('Edit device')) !!}
            @else
                {!! Form::submit('submit', __('Add device')) !!}
            @endif
        </div>
    </div>
    {!! Form::close() !!}

@stop