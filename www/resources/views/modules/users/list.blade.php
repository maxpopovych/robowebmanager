@extends('panel')

@section('content')

    <script>

        window.lang = {
            pagination_info: 'Displaying {from} to {to} of {total} items',
            remove_selected: 'Remove'
        };

    </script>

    <h2>Users</h2>

    <div id="table-list">
        <div class="actions text-right">

            <a href="{{action('UserController@create')}}" class="btn btn-primary">Add new user</a>
        </div>
        <list-table :list-data="listTableArgs"></list-table>
    </div>

    <script>
        var test = {
            url: "{{ $listTable['url'] }}",
            perPage: "{{ $listTable['per_page'] }}",
            searchForm: true,
            fields: [
                {
                    title: 'Id',
                    name: 'id'
                },
                {
                    title: 'Name',
                    name: 'name'
                },
                {
                    title: 'Email',
                    name: 'email'
                },
                {
                    title: 'Firstname',
                    name: 'firstname'
                },
                {
                    title: 'Lastname',
                    name: 'lastname'
                }
            ],
            actions: {
                edit: {
                    show: true,
                    url: "users/%{id}/edit"
                },
                delete: {
                    show: true,
                    url: "users/%{id}"
                },
            },
        };

        new Vue({
            el: '#table-list',
            data: {
                listTableArgs: test
            }
        });
    </script>

@stop