@extends('panel')

@section('content')

    @if(isset($message))
        <div class="alert alert-info" role="alert">
            {{$message}}
        </div>
    @endif

    @if(isset($user))
        <h2>Edit User</h2>
        {!! Form::horizontal($user)->action('UserController@update', ['id' => $user->id])->method('PUT') !!}
    @else
        <h2>Add new user</h2>
        {!! Form::horizontal(null)->action('UserController@store')->method('POST') !!}
    @endif

    <div class="row">
        <div class="col-md-12">
            {!! Form::group()->fieldSize(8)->email('email')->label(__('Email'))->placeholder(__('Email')) !!}
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            {!! Form::group()->fieldSize(8)->text('name')->label(__('Login'))->placeholder(__('Login')) !!}
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            {!! Form::group()->fieldSize(12)->text('firstname')->label(__('First name'))->placeholder(__('First name')) !!}
        </div>
        <div class="col-md-6">
            {!! Form::group()->fieldSize(12)->text('lastname')->label(__('Last name'))->placeholder(__('Last name')) !!}
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            {!! Form::group()->fieldSize(12)->password('password')->label(__('Password'))->placeholder(__('Password')) !!}
        </div>
        <div class="col-md-6">
            {!! Form::group()->fieldSize(12)->password('comfirm_password')->label(__('Confirm password'))->placeholder(__('Confirm password')) !!}
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            {!! Form::group()->fieldSize(8)->input('tel', 'phone')->label(__('Phone'))->placeholder(__('Phone')) !!}
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 text-right">
            @if(isset($user))
                {!! Form::submit('submit', __('Edit user')) !!}
            @else
                {!! Form::submit('submit', __('Add user')) !!}
            @endif
        </div>
    </div>
    {!! Form::close() !!}

@stop