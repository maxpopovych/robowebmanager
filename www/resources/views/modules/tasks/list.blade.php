@extends('panel')

@section('content')

    <script>

        window.lang = {
            pagination_info: 'Displaying {from} to {to} of {total} items',
            remove_selected: 'Remove'
        };

    </script>

    <h2>Tasks</h2>

    <div id="table-list">
        <div class="actions text-right">

            <a href="{{action('TaskController@create')}}" class="btn btn-primary">Add new task</a>
        </div>
        <list-table :list-data="listTableArgs"></list-table>
    </div>

    <script>

        new Vue({
            el: '#table-list',
            data: {
                listTableArgs: {
                    url: "{{ $listTable['url'] }}",
                    perPage: "{{ $listTable['per_page'] }}",
                    searchForm: true,
                    fields: [
                        {
                            title: 'Id',
                            name: 'id'
                        },
                        {
                            title: 'Name',
                            name: 'name'
                        },
                    ],
                    actions: {
                        edit: {
                            show: true,
                            url: "tasks/%{id}/edit"
                        },
                        delete: {
                            show: true,
                            url: "tasks/%{id}"
                        },
                    },
                }
            }
        });
    </script>

@stop