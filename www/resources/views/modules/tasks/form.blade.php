@extends('panel')

@section('content')

    @if(isset($message))
        <div class="alert alert-info" role="alert">
            {{$message}}
        </div>
    @endif

    @if(isset($task))
        <h2>Edit Task</h2>
        {!! Form::horizontal($task)->action('TaskController@update', ['id' => $task->id])->method('PUT') !!}
    @else
        <h2>Add new task</h2>
        {!! Form::horizontal(null)->action('TaskController@store')->method('POST') !!}
    @endif

    <div class="row">
        <div class="col-md-12">
            {!! Form::group()->fieldSize(8)->text('name')->label(__('Name'))->placeholder(__('Name'))->required(true) !!}
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            {!! Form::group()->fieldSize(8)->text('task')->label(__('Task'))->placeholder(__('Task'))->required(true) !!}
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 text-right">
            @if(isset($task))
                {!! Form::submit('submit', __('Edit task')) !!}
            @else
                {!! Form::submit('submit', __('Add task')) !!}
            @endif
        </div>
    </div>
    {!! Form::close() !!}

@stop