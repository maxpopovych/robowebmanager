@extends('body')

@section('body')
    <div class="site-menubar-unfold">
        @include('partials/panel/menubar')
        <div class="page">
            <div class="page-content">
                <div class="panel">
                    <div class="panel-body">
                        @yield('content')
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop