<div class="site-menubar">
    <div class="site-menubar-body">
        <div>
            <div>
                <ul class="site-menu" data-plugin="menu">
                    @foreach (Application::menubar()->getTree() as $item)
                        <li class="site-menu-item @if (!empty($item['childrens'])) has-sub @endif ">
                            <a href="{{$item['item']->url}}">
                                @if(!empty($item['item']->icon))
                                <i class="site-menu-icon {{ $item['item']->icon }}" aria-hidden="true"></i>
                                @endif
                                <span class="site-menu-title">{{ $item['item']->name }}</span>
                            </a>
                            @foreach($item['childrens'] as $children)
                                <ul class="site-menu-sub">
                                    <li class="site-menu-item">
                                        <a class="animsition-link" href="{{$children['item']->url}}">
                                            <span class="site-menu-title">{{$children['item']->name}}</span>
                                        </a>
                                    </li>
                                </ul>
                            @endforeach
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
        <div class="site-menubar-footer">
            <a href="{{action('UserController@logout')}}" data-placement="top" data-toggle="tooltip" data-original-title="Logout">
                Logout
            </a>
        </div>
    </div>
</div>