<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 255);
            $table->string('firstname', 255);
            $table->string('lastname', 255);
            $table->string('email')->unique();
            $table->string('password');
            $table->string('phone', 20);
            $table->string('api_token', 60)->unique();
            /**
             * We have three roles
             * 1 - Headadmin
             * 2 - User
             */
            $table->unsignedInteger('role');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropIfExists();
        });
    }
}
