<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /* Create root user */

        $user = new App\User();
        $user->password = Hash::make('root');
        $user->name = 'root';
        $user->firstname = '';
        $user->lastname = '';
        $user->phone = '';
        $user->role = 1;
        $user->api_token = str_random(60);
        $user->email = 'root@prefectlyproject.pl';
        $user->save();
    }
}
