<?php

namespace App\Extensions\Application\Traits;

trait Tags
{
    /**
     * @param $tag
     * @param $attributes
     * @param bool $closesd
     * @return array
     */
    private function generateHTMLTag($tag, $attributes, $closesd = true) {

        $tags = [];

        foreach ($attributes as $properties) {
            $htmlTag = "<".$tag." ";
            $attirbutes = [];
            foreach ($properties as $property => $value) {
                $value = htmlentities($value, ENT_QUOTES, 'UTF-8', false);
                $attirbutes[] = $property . '="'.$value.'"';
            }
            $htmlTag .= implode(' ', $attirbutes);
            $htmlTag .= $closesd === false ? " />" : "></".$tag.">";
            $tags[] = $htmlTag;
        }

        return $tags;
    }
}