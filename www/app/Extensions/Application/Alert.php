<?php

namespace App\Extensions\Application;


class Alert
{
    private static $stock = [];

    /**
     * Add alert to flash session
     *
     * @param string $type
     * @param string $text
     */
    public static function add(string $type, string $text) {
        self::$stock[] = [ 'type' => $type, 'text' => $text ];
        \session()->flash( 'alert_flash', self::$stock );
    }

    /**
     * Return alert flash
     *
     * @return mixed
     */
    public static function get() : ?array {
        return \session()->get( 'alert_flash' );
    }

    /**
     * Set warning alert
     *
     * @param string $text
     */
    public static function warning(string $text) {
        self::add( 'warning', $text );
    }

    /**
     * Set info alert
     *
     * @param string $text
     */
    public static function info(string $text) {
        self::add( 'info', $text );
    }

    /**
     * Set error alert
     *
     * @param string $text
     */
    public static function error(string $text) {
        self::add( 'success', $text );
    }

    /**
     * Set success alert
     *
     * @param string $text
     */
    public static function success(string $text) {
        self::add( 'success', $text );
    }
}