<?php

namespace App\Extensions\Application\MenuBar;

class MenuItem
{

    /**
     * Keep url to module
     *
     * @var
     */
    protected $url;

    /**
     * Priority in menu
     *
     * @var
     */
    protected $priority;

    /**
     * Name item menu
     *
     * @var
     */
    protected $name;

    /**
     * Handle to child
     *
     * @var
     */
    protected $child;

    /**
     * Class to icon from fontawesome
     *
     * @var
     */
    protected $icon;

    /**
     * Setter
     *
     * @param $name
     * @param $value
     */
    public function __set($name, $value)
    {
        $method = 'set'.ucfirst($name);
        if (method_exists($this, $method)) {
            call_user_func([$this, $method], $value);
        } else {
            $this->$name = $value;
        }
    }

    /**
     * Getter
     *
     * @param $name
     * @return mixed|null
     */
    public function __get($name)
    {
        $return = null;

        $method = 'get'.ucfirst($name);
        if (method_exists($this, $method)) {
            $return = call_user_func([$this, $method]);
        } else {
            $return = $this->$name;
        }

        return $return;
    }

    /**
     * Setter for url
     *
     * @param mixed $url
     */
    public function setUrl($url)
    {
        $this->url = url($url);
    }

    /**
     * Setter for priority
     *
     * @param mixed $priority
     */
    public function setPriority($priority)
    {
        $this->priority = intval($priority);
    }

}