<?php

namespace App\Extensions\Application;

use App\Extensions\Application\Traits\Tags;

class Header
{
    use Tags;

    /**
     * Separator for page title
     *
     * @var string
     */
    protected $titleSeparator = '-';

    /**
     * Keep title page
     *
     * @var array
     */
    protected $title = [];

    /**
     * Keep all meta tags for page
     *
     * @var array
     */
    protected $meta = [];

    /**
     * Keep all scripts for page
     *
     * @var array
     */
    protected $scripts = [];

    /**
     * Keep all links tag for page
     *
     * @var array
     */
    protected $link = [];

    /**
     * Set separator
     *
     * @param $separator
     */
    public function setSeparator($separator) {
        $this->titleSeparator = $separator;
    }

    /**
     * Add link
     *
     * @param $name
     * @param $properties
     */
    public function addLink($name, $properties) {
        $this->link[$name] =  $properties;
    }

    /**
     * Remove link by handle
     *
     * @param $handle
     */
    public function removeLink($handle) {
        if (isset($this->link[$handle])) {
            unset($this->link[$handle]);
        }
    }

    /**
     * Add stylesheet to application
     *
     * @param $handle
     * @param $url
     * @param string $media
     */
    public function addStylesheet($handle, $url, $media = 'all') {
        $this->addLink('style_'.$handle, [
            'rel' => 'stylesheet',
            'href' => $url,
            'type' => 'text/css',
            'media' => $media
        ]);
    }

    /**
     * Remove link by handle
     *
     * @param $handle
     */
    public function removeStylesheet($handle) {
        $this->removeLink('style_'.$handle);
    }

    /**
     * Add script
     *
     * @param $handle
     * @param $url
     */
    public function addScript($handle, $url) {
        $this->scripts[$handle] = [
            'type' => 'text/javascript',
            'src' => $url
        ];
    }

    /**
     * Remove script by $handle
     *
     * @param $handle
     */
    public function removeScript($handle) {
        if(isset($this->scripts[$handle])) {
            unset($this->scripts[$handle]);
        }
    }

    /**
     * Set meta if properties is string will be create array with name and content
     *
     * @param $name
     * @param array|string $properties
     */
    public function setMeta($name, $properties) {

        if (!is_array($properties)) {
            $properties = [
                'name' => $name,
                'content' => $properties
            ];
        }

        $this->meta[$name] =  $properties;
    }

    /**
     * Return meta by name if empty return $default value
     *
     * @param $name
     * @param array $default
     * @return mixed|null
     */
    public function getMeta($name, $default = []) {

        return (isset($this->meta[$name]) ? $this->meta[$name] : $default);
    }

    /**
     * Generate meta
     *
     * @return string
     */
    public function generateMetaTags() {

        return $this->generateHTMLTag('meta', $this->meta, false);
    }

    /**
     * @return array
     */
    public function generateLinkTags() {

        return $this->generateHTMLTag('link', $this->link, false);
    }

    /**
     * @return array
     */
    public function generateScriptTags() {

        return $this->generateHTMLTag('script', $this->scripts, true);
    }

    /**
     * Set title
     *
     * @param $title
     * @param int $priority
     */
    public function setTitle($title, $priority = 0) {

        $this->title[$priority] = $title;
    }

    /**
     * @return string
     */
    public function getTitle() {

        return implode(' '.$this->titleSeparator.' ', $this->title);
    }

    /**
     * Render title tag
     *
     * @return string
     */
    public function getTitleTag() {

        return "<title>".$this->getTitle().'</title>';
    }

    /**
     * Render all tags for head
     *
     * @return string
     */
    public function generate() {

        $tags = [];
        $tags[] = $this->getTitleTag();
        $tags = array_merge($tags, $this->generateMetaTags());
        $tags = array_merge($tags, $this->generateLinkTags());
        $tags = array_merge($tags, $this->generateScriptTags());

        return implode("\n\t", $tags);
    }

}