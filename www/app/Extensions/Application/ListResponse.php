<?php

namespace App\Extensions\Application;


class ListResponse
{

    protected $success = false;

    protected $error = '';

    /**
     * @param bool $success
     */
    public function setSuccess( $success )
    {
        $this->success = (bool) $success;
    }

    /**
     * @param string $error
     */
    public function setError( $error )
    {
        $this->error = $error;
    }

    /**
     * @return string
     */
    public function isError()
    {
        return !empty($this->error);
    }

    public function getResponse() {

        return [
            'success' => (bool) $this->success,
            'error' => $this->error
        ];
    }

}