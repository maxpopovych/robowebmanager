<?php

namespace App\Extensions\Application;

class ListTable
{

    private $columns = ['id', 'name', 'email'];

    private $orderBy = null;

    private $order = 'asc';

    private $perPage = 10;

    private $page = 1;

    private $model = '';

    private $apiUrl = '';

    public $where = [];

    public $pmodifyQuery = null;

    /**
     * Setter
     *
     * @param $name
     * @param $value
     */
    public function __set($name, $value) {
        $method = 'set'.ucfirst($name);
        if (method_exists($this, $method)) {
            call_user_func([$this, $method], $value);
        } else {
            $this->$name = $value;
        }
    }

    /**
     * Getter
     *
     * @param $name
     * @return mixed|null
     */
    public function __get($name) {
        $return = null;

        $method = 'get'.ucfirst($name);
        if (method_exists($this, $method)) {
            $return = call_user_func([$this, $method]);
        } else {
            $return = $this->$name;
        }

        return $return;
    }

    /**
     * Setter for sort
     *
     * @param $sort
     * @return bool
     */
    public function setSort($sort) {

        if (empty($sort) || !strpos($sort, '|')) {
            return false;
        }

        list($orderBy, $order) = explode('|', $sort);

        $this->setOrderBy($orderBy);
        $this->setOrder($order);

    }

    /**
     * Setter for api url
     *
     * @param $url
     */
    public function setApiUrl($url) {

        $this->apiUrl = url($url);

    }

    /**
     * Setter for property orderBy
     *
     * @param $orderBy
     */
    public function setOrderBy($orderBy) {

        if (in_array($orderBy, $this->columns)) {
            $this->orderBy = $orderBy;
        }

    }

    /**
     * Setter for property order
     *
     * @param $order
     */
    public function setOrder($order) {

        if (in_array($order, ['asc', 'desc'])) {
            $this->order = $order;
        }

    }

    /**
     * Setter for property perPage
     *
     * @param $perPage
     */
    public function setPerPage($perPage) {

        $this->perPage = intval($perPage);

    }

    /**
     * Setter for property page
     *
     * @param $page
     */
    public function setPage($page) {

        $this->page = intval($page);

    }


    /**
     * Setter for model
     *
     * @param $model
     */
    public function setModel($model) {

        if (class_exists($model)) {
            $this->model = $model;
        }

    }

    /**
     * Setter for columns
     *
     * @param array $columns
     */
    public function setColumns(array $columns) {

        $this->columns = $columns;

    }

    public function setWhere(array $where) {

        $this->where = $where;
    }

    public function modifyQuery($function) {

        $this->pmodifyQuery = $function;
    }

    /**
     * Array response for list
     *
     * @return array
     */
    public function getResponse() {

        $response = [];

        $modelInstance = new $this->model;

        $query = $modelInstance->getQuery();

        if ( !empty( $this->where ) ) {
            $query->where($this->where);
        }

        if ( !empty($this->orderBy) ) {
            $query = $query->orderBy($this->orderBy, $this->order);
        }

        if ( !empty( $this->pmodifyQuery ) ) {
            call_user_func($this->pmodifyQuery, $query);
        }

        /** @var @todo change columns */
        $paginate = $query->paginate($this->perPage, $this->columns, 'page', $this->page)->toArray();

        $response['total'] = $paginate['total'];
        $response['data'] = $paginate['data'];
        $response['current_page'] = $paginate['current_page'];
        $response['per_page'] = $paginate['per_page'];
        $response['last_page'] = $paginate['last_page'];
        $response['from'] = $paginate['from'];
        $response['to'] = $paginate['to'];

        return $response;

    }

    /**
     * Return object for list table view
     */
    public function getParams() {

        $params = [];
        $params['per_page'] = $this->perPage;
        $params['url'] = $this->apiUrl;

        return $params;

    }

}