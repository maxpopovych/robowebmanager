<?php

namespace App\Extensions\Application;

use App\Extensions\Application\MenuBar\MenuItem;
use Mockery\Exception;

class MenuBar
{

    protected $menu = [];

    /**
     * Add menu to menu bar
     *
     * @param $handle
     * @param array $attrs
     */
    public function addMenu($handle, $attrs = []) {

        $defaults = [
            'name' => '',
            'priority' => 0,
            'url' => '',
            'child' => null,
            'icon' => ''
        ];

        $attrs = array_merge($defaults, $attrs);

        if (empty($attrs['name'])) {
            throw new Exception('Field name is required');
        }

        if (empty($attrs['url'])) {
            throw new Exception('Field url is required');
        }

        $menuItem = new MenuItem();
        $menuItem->priority = $attrs['priority'];
        $menuItem->name = $attrs['name'];
        $menuItem->url = 'panel/'.ltrim($attrs['url'], '/');
        $menuItem->child = $attrs['child'];
        $menuItem->icon = $attrs['icon'];

        $this->menu[$handle] = $menuItem;
    }

    /**
     * Generate tree menu
     *
     * @param null $child
     * @return array
     */
    public function getTree($child = null) {

        $tree = [];

        foreach($this->menu as $handle => $menu) {

            if ($child == $menu->child) {
                $tree[] = [
                    'item' => $menu,
                    'childrens' => $this->getTree($handle)
                ];
            }
        }

        return $tree;

    }

}