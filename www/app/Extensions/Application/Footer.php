<?php

namespace App\Extensions\Application;

use App\Extensions\Application\Traits\Tags;

class Footer
{
    use Tags;

    /**
     * Keep all scripts for page
     *
     * @var array
     */
    protected $scripts = [];

    /**
     * Add script
     *
     * @param $handle
     * @param $url
     */
    public function addScript($handle, $url) {
        $this->scripts[$handle] = [
            'type' => 'text/javascript',
            'src' => $url
        ];
    }

    /**
     * Remove script by $handle
     *
     * @param $handle
     */
    public function removeScript($handle) {
        if(isset($this->scripts[$handle])) {
            unset($this->scripts[$handle]);
        }
    }

    /**
     *
     *
     * @return array
     */
    public function generateScriptTags() {

        return $this->generateHTMLTag('script', $this->scripts, true);
    }

    /**
     * Render all tags for head
     *
     * @return string
     */
    public function generate() {

        $tags = [];
        $tags = array_merge($tags, $this->generateScriptTags());

        return implode("\n\t", $tags);
    }
}