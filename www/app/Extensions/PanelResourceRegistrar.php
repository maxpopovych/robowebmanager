<?php

namespace App\Extensions;

use \Illuminate\Routing\ResourceRegistrar;

class PanelResourceRegistrar extends ResourceRegistrar {

    /**
     * The default actions for a resourceful controller.
     *
     * @var array
     */
    protected $resourceDefaults = ['index', 'items', 'create', 'store', 'show', 'edit', 'update', 'destroy'];

    /**
     * The verbs used in the resource URIs.
     *
     * @var array
     */
    protected static $verbs = [
        'create' => 'create',
        'edit' => 'edit',
        'items' => 'items'
    ];

    /**
     * Add the items method for a resourceful route.
     *
     * @param  string  $name
     * @param  string  $base
     * @param  string  $controller
     * @param  array   $options
     * @return \Illuminate\Routing\Route
     */
    protected function addResourceItems($name, $base, $controller, $options)
    {
        $uri = $this->getResourceUri($name).'/'.static::$verbs['items'];

        $action = $this->getResourceAction($name, $controller, 'items', $options);

        return $this->router->get($uri, $action);
    }

    /**
     * @todo dorobic metode do artisana z generowaniem kontrolera z resource
     */

}