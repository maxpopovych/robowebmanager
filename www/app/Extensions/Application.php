<?php

namespace App\Extensions;

class Application
{

    /**
     * Keep all instances
     *
     * @var array
     */
    public static $instances = [];

    /**
     * Return instance to $class
     *
     * @param $class
     * @return mixed
     */
    public static function getInstance($class) {

        $instance = null;

        if (!isset(self::$instances[$class]) || !(self::$instances[$class] instanceof $class)) {
            self::$instances[$class] = new $class;
        }

        return self::$instances[$class];

    }

    /**
     * Return instance to application header
     *
     * @return \App\Extensions\Application\Header
     */
    public static function head() {
        return self::getInstance('\App\Extensions\Application\Header');
    }

    /**
     * Return instance to application footer
     *
     * @return \App\Extensions\Application\Footer
     */
    public static function footer() {
        return self::getInstance('\App\Extensions\Application\Footer');
    }

    /**
     * Return instance to application menubar
     *
     * @return \App\Extensions\Application\MenuBar
     */
    public static function menubar() {
        return self::getInstance('\App\Extensions\Application\MenuBar');
    }

    /**
     * Return instance to application menubar
     *
     * @return \App\Extensions\Application\Page
     */
    public static function page() {
        return self::getInstance('\App\Extensions\Application\Page');
    }

    /**
     * Return instance to application menubar
     *
     * @return \App\Extensions\Application\Page
     */
    public static function alert() {
        return self::getInstance('\App\Extensions\Application\Alert');
    }
}