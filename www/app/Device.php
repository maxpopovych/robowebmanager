<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Device extends Model
{
    protected $table = 'devices';

    protected $guarded = [ 'id' ];

    public function scopeGetWithTask($query) {
        return $query->leftJoin('tasks', 'tasks.id', '=', 'devices.id_active_task');
    }
}