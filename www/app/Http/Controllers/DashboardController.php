<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function actionShowDashboard() {

        return view('modules/dashboard');

    }
}
