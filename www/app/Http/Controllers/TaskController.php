<?php

namespace App\Http\Controllers;

use App\Extensions\Application\ListResponse;
use App\Extensions\Application\ListTable;
use App\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;

class TaskController extends Controller
{

    public $listTable;

    public function __construct() {

        $this->listTable = new ListTable();
        $this->listTable->setColumns(['id', 'name']);
        $this->listTable->setApiUrl('panel/tasks/items');
        $this->listTable->setPerPage(10);
        $this->listTable->setModel(Task::class);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('modules/tasks/list')->with(['listTable' => $this->listTable->getParams()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view( 'modules/tasks/form' );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $task = new Task();

        $task->fill([
            'name' => $request->input('name', ''),
            'task' => $request->input('task', ''),
            'id_user' => Auth::id()
        ]);

        $task->save();

        if ($task->save()) {
            Session::flash('message', __('Added success fully'));
        }

        return redirect()->action('TaskController@edit', [ 'id' => $task->id ]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $task = Task::find($id);

        if ( empty($task) || $task->id_user !== Auth::id() ) {
            $message = Session::flash('Access denided');
            return redirect()->action('TaskController@index')->with('message', $message);
        }

        $message = Session::get('message');
        return view( 'modules/tasks/form' )->with( 'task', $task)->with('message', $message);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $task = Task::find($id);

        $task->fill([
            'name' => $request->input('name', ''),
            'task' => $request->input('task', ''),
            'id_user' => Auth::id()
        ]);

        $task->save();

        if ($task->save()) {
            Session::flash('message', __('Updated success fully'));
        }

        return redirect()->action('TaskController@edit', [ 'id' => $task->id ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $listReponse = new ListResponse();

        $taskToDelete = Task::where('id', (int) $id)->where('id_user', Auth::id())->first();

        if (!$listReponse->isError()) {
            if ($taskToDelete->delete()) {
                $listReponse->setSuccess(true);
            } else {
                $listReponse->setError('Ups coś poszło nie tak.');
            }
        }

        $this->listTable->setWhere([
            [ 'id_user', '=', Auth::id() ]
        ]);

        return response()->json($listReponse->getResponse());
    }

    public function items(Request $request) {

        $this->listTable->setWhere([
            [ 'id_user', '=', Auth::id() ]
        ]);

        $this->listTable->setSort($request->input('sort', null));
        $this->listTable->setPage($request->input('page', 1));

        return response()->json($this->listTable->getResponse());
    }

    public function apiList(Request $request)
    {
        $user = Auth::guard('api')->user();
        $tasks = Task::where('id_user', '=', $user['id'])->get()->toArray();

        return response()->json($tasks);

    }
}
