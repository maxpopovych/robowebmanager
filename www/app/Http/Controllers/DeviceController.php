<?php

namespace App\Http\Controllers;

use App\Device;
use App\Extensions\Application\ListResponse;
use App\Extensions\Application\ListTable;
use App\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Mockery\Exception;
use WebSocket\Client;

class DeviceController extends Controller
{
    public $listTable;

    public function __construct() {

        $this->listTable = new ListTable();
        $this->listTable->setColumns(['devices.id', 'devices.name', 'tasks.name as active_task_name']);
        $this->listTable->setApiUrl('panel/devices/items');
        $this->listTable->setPerPage(10);
        $this->listTable->setModel(Device::class);
        $this->listTable->modifyQuery( function($query) {
            $query->leftJoin('tasks', 'tasks.id', '=', 'devices.id_active_task');
        } );

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tasks = Task::all()->where('id_user', Auth::id());

        return view('modules/devices/list')->with(['listTable' => $this->listTable->getParams(), 'tasks' => $tasks->toArray()]);
    }

    public function items(Request $request) {
        $this->listTable->setWhere([
            [ 'devices.id_user', '=', Auth::id() ]
        ]);

        $this->listTable->setSort($request->input('sort', null));
        $this->listTable->setPage($request->input('page', 1));

        return response()->json($this->listTable->getResponse());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view( 'modules/devices/form' );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $device = new Device();

        $device->fill([
            'name' => $request->input('name', ''),
            'id_user' => Auth::id()
        ]);

        $device->save();

        if ($device->save()) {
            Session::flash('message', __('Added success fully'));
        }

        return redirect()->action('DeviceController@edit', [ 'id' => $device->id ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $device = Device::find($id);

        if ( empty($device) || $device->id_user !== Auth::id() ) {
            $message = Session::flash('Access denided');
            return redirect()->action('DeviceController@index')->with('message', $message);
        }

        $message = Session::get('message');
        return view( 'modules/devices/form' )->with( 'device', $device)->with('message', $message);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $device = Device::find($id);

        $device->fill([
            'name' => $request->input('name', ''),
            'id_user' => Auth::id()
        ]);

        $device->save();

        if ($device->save()) {
            Session::flash('message', __('Updated success fully'));
        }

        return redirect()->action('DeviceController@edit', [ 'id' => $device->id ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $listReponse = new ListResponse();

        $deviceToDelete = Device::where('id', (int) $id)->where('id_user', Auth::id())->first();

        if (!$listReponse->isError()) {
            if ($deviceToDelete->delete()) {
                $listReponse->setSuccess(true);
            } else {
                $listReponse->setError('Ups coś poszło nie tak.');
            }
        }

        $this->listTable->setWhere([
            [ 'id_user', '=', Auth::id() ]
        ]);

        return response()->json($listReponse->getResponse());
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function apiList( Request $request) {

        $user = Auth::guard('api')->user();

        $devices = Device::where('id_user', $user['id'])->get()->toArray();

        return response()->json($devices);
    }

    /**
     * @param Request $request
     * @param $id_device
     * @return \Illuminate\Http\JsonResponse
     */
    public function apiActionTask( Request $request, $id_device)
    {
        $response = [
            'error' => false,
            'success' => false
        ];

        $user = Auth::guard('api')->user();

        $device = Device::where([
            [ 'id_user', $user['id'] ],
            [ 'id',  $id_device ]
        ])->first();

        if ( empty($device) ) {
            $response['error'] = true;
            return response()->json($response);
        }

        $action = $request->input('action', 'start');

        if (!in_array($action, [ 'start', 'stop' ])) {
            $response['error'] = true;
            return response()->json($response);
        }

        $taskId = $request->input('id_task', null);

        if ($action == 'stop') {
            $taskId = null;
        }

        $device->id_active_task = $taskId;

        try {
            $device->save();
            $client = new Client("ws://" . env('WEBSOCKET_HOST') . "/status?id_user=".$user['id']."&id_device=".$id_device);
            $client->send(json_encode([
                    'task_id' => $taskId,
                    'location' => [
                        'lat' => null,
                        'lng' => null,
                    ]
                ])
            );
        } catch (\Exception $e) {
            $response['error'] = true;
            return response()->json($response);
        }

        $response['success'] = true;
        return response()->json($response);

    }

    public function setTask(Request $request) {

        $idUser = Auth::id();
        $device = Device::find($request->input('id_device'));
        $device->id_active_task = $request->input('id_task');

        $listReponse = new ListResponse();

        try {
            $device->save();
            $client = new Client("ws://" . env('WEBSOCKET_HOST') . "/status?id_user=".$idUser."&id_device=1");
            $client->send(json_encode([
                    'task_id' => $device->id_active_task,
                    'location' => [
                        'lat' => null,
                        'lng' => null,
                    ]
                ])
            );
            $listReponse->setSuccess(true);
        } catch (\Exception $e) {
            $response['error'] = true;
            return response()->json($response);
        }

        return response()->json($listReponse->getResponse());
    }

    /**
     * @param Request $request
     * @param $id_device
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function apiGetActiveTask( Request $request, $id_device)
    {
        $user = Auth::guard('api')->user();

        $device = Device::where([
            [ 'devices.id_user', $user['id'] ],
            [ 'devices.id',  $id_device ]
        ])->join('tasks', 'devices.id_active_task', '=', 'tasks.id')->value('task');

        return response($device);

    }
}
