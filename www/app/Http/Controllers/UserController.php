<?php

namespace App\Http\Controllers;

use App\Extensions\Application\ListResponse;
use App\User;
use Illuminate\Http\Request;
use App\Extensions\Application\ListTable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class UserController extends Controller
{

    public $listTable;

    public function __construct() {

        $this->listTable = new ListTable();
        $this->listTable->setColumns(['id', 'name', 'email', 'firstname', 'lastname']);
        $this->listTable->setApiUrl('panel/users/items');
        $this->listTable->setPerPage(10);
        $this->listTable->setModel(User::class);

    }

    public function role(  )
    {
//        return [
//            'list' => 'Pokazywać liste',
//            'delete' => 'Usuwać użytkowników',
//            'edit' => 'Edytować użytkowników',
//            'create' => 'Tworzenie nowych użytkowników'
//        ];
    }

    public function create() {

        return view( 'modules/users/form' );
    }

    public function index() {

        return view('modules/users/list')->with(['listTable' => $this->listTable->getParams()]);

    }

    public function actionEditUser() {

        //return view('modules/users/form');

    }

    public function items(Request $request) {

        $this->listTable->setSort($request->input('sort', null));
        $this->listTable->setPage($request->input('page', 1));

        return response()->json($this->listTable->getResponse());
    }

    public function edit($id) {

        $message = Session::get('message');

        return view( 'modules/users/form' )->with( 'user', User::find((int) $id))->with('message', $message);
    }

    public static function store(Request $request) {

        $user = new User();

        $user->firstname = $request->input('firstname');
        $user->lastname = $request->input('lastname');
        $user->email = $request->input('email');
        $user->name = $request->input('name', '');
        $user->phone = $request->input('phone', '');
        $user->api_token = str_random(60);
        $user->role = 1;

        $fieldPassowrd = $request->input('password');
        $fieldConfirmPassword = $request->input('comfirm_password');

        if (!empty($fieldPassowrd) && !empty($fieldConfirmPassword) && $fieldPassowrd === $fieldConfirmPassword) {
            $user->password = Hash::make($fieldPassowrd);
        }

        if ($user->save()) {
            Session::flash('message', __('Update success fully'));
        }

        return redirect()->action('UserController@edit', [ 'id' => $user->id ]);
    }

    public function update(Request $request , $id_user) {

        $id = intval($id_user);

        $user = User::find($id_user);

        $user->firstname = $request->input('firstname');
        $user->lastname = $request->input('lastname');
        $user->email = $request->input('email');
        $user->phone = $request->input('phone', '');

        $fieldPassowrd = $request->input('password');
        $fieldConfirmPassword = $request->input('comfirm_password');

        if (!empty($fieldPassowrd) && !empty($fieldConfirmPassword) && $fieldPassowrd === $fieldConfirmPassword) {
            $user->password = Hash::make($fieldPassowrd);
        }

        if ($user->save()) {
            Session::flash('message', __('Update success fully'));
        }

        return redirect()->action('UserController@edit', [ 'id' => $user->id ]);

    }

    public function destroy( $id )
    {
        $listReponse = new ListResponse();

        $user = \Auth::user();
        $userToDelete = User::find((int) $id);

        if ($user->id === $userToDelete->id) {
            $listReponse->setError('You can\'t delete own user.');
        }

        if (!$listReponse->isError()) {
            if ($userToDelete->delete()) {
                $listReponse->setSuccess(true);
            } else {
                $listReponse->setError('Ups coś poszło nie tak.');
            }
        }

        return response()->json($listReponse->getResponse());

    }

    public function logout() {

        Auth::logout();
        return redirect('/');
    }
}
