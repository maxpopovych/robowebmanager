<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\DB;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/panel';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Show login form
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showLoginForm()
    {
        return view('login');
    }

    public function authenticate(Request $request)
    {
        $response = [
            'success' => false,
            'redirect' => '',
            'error' => [],
        ];

        $login = $request->input('login');
        $password = $request->input('password');
        $rememberMe = $request->input('rememberMe');

        if (Auth::attempt(['email' => $login, 'password' => $password], $rememberMe)) {
            $response['success'] = true;
            $response['redirect'] = url($this->redirectTo);
        } else {
            $response['error'] = __('Invalid login or password');
        }

        return response()->json($response);

    }

    public function apiLogin(Request $request)
    {
        $response = [
            'success' => false,
            'redirect' => '',
            'error' => [],
        ];

        $login = $request->input('login');
        $password = $request->input('password');

        if (!empty($login) && !empty($password) && Auth::attempt(['email' => $login, 'password' => $password])) {
            $user = Auth::user();
            $response['success'] = true;
            $response['api_token'] = $user['api_token'];
        } else {
            $response['error'] = __('Invalid login or password');
        }

        return response()->json($response);
    }
}
