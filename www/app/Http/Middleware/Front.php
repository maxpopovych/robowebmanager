<?php

namespace App\Http\Middleware;

use Closure;
use App\Extensions\Application;

class Front
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        Application::head()->addStylesheet('front', asset('css/front.css'));
        Application::footer()->addScript('front', asset('js/front.js'));

        return $next($request);
    }
}
