<?php

namespace App\Http\Middleware;

use Closure;
use App\Extensions\Application;

class Panel
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        Application::head()->addStylesheet('panel', asset('css/panel.css'));
        Application::head()->addScript('panel', asset('js/panel.js'));

        $this->createMenuBar();

        return $next($request);
    }

    public function createMenuBar() {

        Application::menubar()->addMenu('dashboard',[
            'name' => __('Dashboard'),
            'url' => '/'
        ]);

        Application::menubar()->addMenu('users',[
            'name' => __('Users'),
            'url' => '/users',
        ]);

        Application::menubar()->addMenu('tasks',[
            'name' => __('Tasks'),
            'url' => '/tasks',
        ]);

        Application::menubar()->addMenu('devices',[
            'name' => __('Devices'),
            'url' => '/devices',
        ]);

    }
}
