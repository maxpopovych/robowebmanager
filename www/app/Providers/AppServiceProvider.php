<?php

namespace App\Providers;

use App\Extensions\Application;
use App\Extensions\PanelResourceRegistrar;
use Illuminate\Support\Facades\App;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        Application::head()->setMeta('charset', ['charset' => 'utf-8']);
        Application::head()->setMeta('iecomatible', [
            'http-equiv' => 'X-UA-Compatible',
            'viewport' => 'IE=edge'
        ]);
        Application::head()->setMeta('viewport', [
            'http-equiv' => 'viewport',
            'viewport' => 'width=device-width, initial-scale=1'
        ]);

        Application::head()->setTitle('MyTransfer');


    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(\Illuminate\Routing\ResourceRegistrar::class, \App\Extensions\PanelResourceRegistrar::class );
    }
}
