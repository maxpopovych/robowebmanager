const http = require('http');
const WebSocket = require('ws');
const url = require('url');

const server = http.createServer();
const webSocketServer = new WebSocket.Server({ noServer: true });

webSocketServer.on('connection', function(ws) {
    ws.on('message', function(message) {
        webSocketServer.clients.forEach(function(client) {
            if (client.idDevice === ws.idDevice && client.idUser === ws.idUser && client !== ws) {
                client.send(message);
            }
        })
    })
});

server.on('upgrade', function upgrade(request, socket, head) {
    var socketUrl =url.parse(request.url, true);

    var idUser = socketUrl.query.id_user | null;
    var idDevice = socketUrl.query.id_device | null;
    var pathname = socketUrl.pathname;

    if (pathname === '/status' && idUser && idDevice)   {
        webSocketServer.handleUpgrade(request, socket, head, function done(ws) {
            ws.idUser = idUser;
            ws.idDevice = idDevice;
            webSocketServer.emit('connection', ws, request);
        });
    }  else {
        socket.destroy();
    }
});

server.listen(6001);