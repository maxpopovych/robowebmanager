<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/login', 'Auth\LoginController@apiLogin');

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::group(['middleware' => ['auth:api']], function ($router) {
    Route::get('device/list', 'DeviceController@apiList');
    Route::get('task/list', 'TaskController@apiList');
    Route::post('device/{id_device}/action', 'DeviceController@apiActionTask');
    Route::get('device/{id_device}/action', 'DeviceController@apiGetActiveTask');
});