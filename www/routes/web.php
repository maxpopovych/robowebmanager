<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['front','guest']], function ($router) {
    Route::get('/', 'Auth\LoginController@showLoginForm');
    Route::post('/authenticate', 'Auth\LoginController@authenticate');
});


Route::group(['prefix' => 'panel', 'middleware' => ['panel','auth']], function ($router) {

    Route::get('/', 'DashboardController@actionShowDashboard');
    /**
     * Users
     */
    Route::resource('users', 'UserController');
    Route::get('user/logout', 'UserController@logout');

    Route::resource('devices', 'DeviceController');
    Route::post('devices/set_task', 'DeviceController@setTask');

    Route::resource('tasks', 'TaskController');
});